<?php

use Illuminate\Database\Seeder;
// Import model of user
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = array ([
        	'name' => 'Usuario Prueba', 
        	'email' => 'usuario_prueba@exampleGradiWeb.com', 
        	'password' => bcrypt('prueba')
        ]);
        foreach ($users as $value) {
	        $user = new User;
	        $user->name = $value['name'];
	        $user->email = $value['email'];
	        $user->password = $value['password'];
	        $user->save(); 
        }
    }
}
