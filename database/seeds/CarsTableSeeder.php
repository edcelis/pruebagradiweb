<?php

use Illuminate\Database\Seeder;
//Import model of Car
use App\Models\Car; 

class CarsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cars = array ([
        	'licensePlate' => 'SVG-896', 
        	'typeCar' => 'Automóvil', 
        	'mark' => 'Chevrolet', 
        	'description' => 'Descripción de prueba',
        	'client_id' => 1
        ],
    	[
        	'licensePlate' => 'CSS-596', 
        	'typeCar' => 'Camion', 
        	'mark' => 'Chevrolet', 
        	'description' => 'Descripción de prueba',
        	'client_id' => 2
        ]);
        foreach ($cars as $value) {
	        $car = new Car;
	        $car->licensePlate = $value['licensePlate'];
	        $car->typeCar = $value['typeCar'];
	        $car->mark = $value['mark'];
	        $car->description = $value['description'];
	        $car->client_id = $value['client_id'];
	        $car->save(); 
        }
    }
}
