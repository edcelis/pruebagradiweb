<?php

use Illuminate\Database\Seeder;
//Import model of Client
use App\Models\Client;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clients = array ([
        	'name' => 'Cliente prueba N°1', 
        	'numberDocument' => '1234567890', 
        	'description' => 'Descripción de prueba'
        ],
    	[
        	'name' => 'Cliente prueba N°2', 
        	'numberDocument' => '0987654321', 
        	'description' => 'Descripción de prueba'
        ]);
        foreach ($clients as $value) {
	        $client = new Client;
	        $client->name = $value['name'];
	        $client->numberDocument = $value['numberDocument'];
	        $client->description = $value['description'];
	        $client->save(); 
        }
    }
}
