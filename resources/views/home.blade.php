@extends('layouts.app')

@section('content')
<div class="container">
    <br>
    <md-empty-state
        md-rounded
        md-icon="verified"
        md-label="¡Hola {{ Auth::user()->name }}!"
        md-description="Selecciona una de las opciones del menú para iniciar con la interacción.">
    </md-empty-state>
</div>
@endsection
