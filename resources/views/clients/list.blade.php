@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row text-center">
			<article class="col-md-12 newSect text-center">
				<h3>Listado de clientes</h3><br>	
				<a href="{{ route('client.create') }}" class="btn btn-primary text-light"><md-icon class="text-light">add_box</md-icon> Nuevo Cliente</a>
			</article>
			<article class="col-md-12">
				<div class="table-responsive text-center">
					<table class="table table-striped table-bordered table-hover" id="table_client">
						<thead>
							<tr>
								<th>Nombre</th>
								<th>Número de documento</th>
								<th>Descripción</th>
								<th>Acción</th>
							</tr>
						</thead>
						<tbody>
							@foreach($clients as $client)
								<tr>
									<td><p>{{ $client->name }}</p></td>
									<td><p>{{ $client->numberDocument }}</p></td>
									<td><p>{{ $client->description }}</p></td>	
									<td>
										<a href="{{ route('client.edit', ['id' => $client->id]) }}" class="btn btn-info text-light"><md-icon class="text-light">create</md-icon> Editar</a>
										<a href="{{ route('client/destroy', ['id' => $client->id]) }}" class="btn btn-danger text-light"><md-icon class="text-light">delete_forever</md-icon> Eliminar</a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</article>
		</div>
	</section>
@endsection()