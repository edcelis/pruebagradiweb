@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-3"></article>
			<article class="col-md-6 text-center">
				<div class="newSect"><h3>Actualizacion de clientes</h3></div>
				{!! Form::model($client, ['route' => ['client.update', $client->id], 'method' => 'PUT']) !!}
					<div class="form-group">
						<label>Nombre del cliente</label>
						<input type="text" name="name" class="form-control" id="name" required maxlength="100" value="{{ $client->name }}">
					</div>
					<div class="form-group">
						<label>Número de documento</label>
						<input type="tel" name="numberDocument" class="form-control" id="numberDocument" required maxlength="15" minlength="5" value="{{ $client->numberDocument }}">
					</div>
					<div class="form-group">
						<label>Descripción</label>
						<textarea name="description" class="form-control" id="description" required maxlength="250">{{ $client->description }}</textarea>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-success"><md-icon class="text-light">save</md-icon> Guardar información</button>
					</div>
				{!! Form::close() !!}
			</article>
			<article class="col-md-3"></article>
		</div>
	</section>
@endsection()