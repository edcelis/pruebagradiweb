@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-12 newSect text-center">
				<h3>Listado de vehículos</h3><br>
				<a href="{{ route('car.create') }}" class="btn btn-primary text-light"><md-icon class="text-light">add_box</md-icon> Nuevo vehículo</a>
			</article>
			<article class="col-md-12">
				<div class="table-responsive text-center">
					<table class="table table-striped table-bordered table-hover" id="table_car">
						<thead>
							<tr>
								<th>Placa</th>
								<th>Marca</th>
								<th>Tipo</th>
								<th>Descripción</th>
								<th>Acción</th>
							</tr>
						</thead>
						<tbody>
							@foreach($cars as $car)
								<tr>
									<td><p class="mayus">{{ $car->licensePlate }}</p></td>
									<td><p>{{ $car->mark }}</p></td>
									<td><p>{{ $car->typeCar }}</p></td>
									<td><p>{{ $car->description }}</p></td>	
									<td>
										<a href="{{ route('car.edit', ['id' => $car->id]) }}" class="btn btn-info text-light"><md-icon class="text-light">create</md-icon> Editar</a>
										<a href="{{ route('car/destroy', ['id' => $car->id]) }}" class="btn btn-danger text-light"><md-icon class="text-light">delete_forever</md-icon> Eliminar</a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</article>
		</div>
	</section>
@endsection()