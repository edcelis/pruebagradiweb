@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-3"></article>
			<article class="col-md-6 text-center">
				<div class="newSect"><h3>Actualización de vehículos</h3></div>
				{!! Form::model($car ,['route' => ['car.update', $car->id], 'method' => 'PUT']) !!}
					<div class="form-group">
						<label>Placa del vehículo</label>
						<input type="text" name="licensePlate" class="form-control" id="name" required maxlength="6" value="{{ $car->licensePlate }}">
					</div>
					<div class="form-group">
						<label>Tipo del vehículo</label>
						<select class="form-control" name="typeCar" id="typeCar" required>
							<option selected value="{{ $car->typeCar }}">{{ $car->typeCar }}</option>
							<option value="Automóvil">Automóvil</option>
							<option value="Autobus">Autobus</option>
							<option value="Camion">Camion</option>
							<option value="Camioneta">Camioneta</option>
							<option value="Furgón">Furgón</option>
						</select>
					</div>
					<div class="form-group">
						<label>Marca del vehículo</label>
						<input type="text" class="form-control" name="mark" id="mark" required maxlength="100" value="{{ $car->mark }}">
					</div>
					<div class="form-group">
						<label>Propietario del vehículo</label>
						<select class="form-control" name="client_id" id="client_id" required>
							<option selected value="{{ $car->client_id }}">{{ $car->client_id }}</option>
							@foreach($clients as $client)
								<option value="{{ $client->id }}">{{ $client->id }} - {{ $client->name }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label>Descripción</label>
						<textarea name="description" class="form-control" id="description" required maxlength="250">{{ $car->description }}</textarea>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-success"><md-icon class="text-light">save</md-icon> Guardar información</button>
					</div>
				{!! Form::close() !!}
			</article>
			<article class="col-md-3"></article>
		</div>
	</section>
@endsection()