
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import Jquery from 'jquery';

require('./bootstrap');
require('./events');

window.Vue = require('vue');
window.dt = require( 'datatables.net');

import VueMaterial from 'vue-material';
//import 'vue-material/dist/vue-material.min.css'
//import 'vue-material/dist/theme/default.css'

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.use(VueMaterial)

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('logica', require('./components/LogicaComponent.vue'));
Vue.component('app', require('./components/AppComponent.vue'));
Vue.component('car', require('./components/CarComponent.vue'));

const app = new Vue({
    el: '#app'
});
