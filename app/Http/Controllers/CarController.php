<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// Import Model of Car
use App\Models\Car as Car;
// Import Model of Client
use App\Models\Client as Client;


class CarController extends Controller
{
    //Method to create
    public function store(Request $request)
    {
    	$car = new Car;
    	$car->create($request->all());
    	return redirect('car');
    }
    //Method to update
    public function update($id, Request $request)
    {
        $car = Car::find($id);
        $car->licensePlate = $request->licensePlate;
        $car->typeCar = $request->typeCar;
        $car->mark = $request->mark;
        $car->description = $request->description;
        $car->client_id = $request->client_id;
        $car->save();
        return redirect('car');
    }
    //Method to redirect to view create
    public function create(){
        $clients = Client::all();
    	return \View::make('cars/new', compact('clients'));
    }
    //Method to redirect to view list
    public function index(){
        $cars = Car::all();
        return \View::make('cars/list', compact('cars'));
    }
    //Method to redirect to view update with the information of car
    public function edit($id){
        $car = Car::find($id);
        $clients = Client::all();
        return \View::make('cars/update', compact('car', 'clients'));
    }
    //Method to delete
    public function destroy($id){
        $car = Car::find($id);
        $car->delete();
        return redirect()->back();
    }
}
