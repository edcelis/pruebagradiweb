<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// Import Model of Client
use App\Models\Client as Client;

class ClientController extends Controller
{
    //Method to create
    public function store(Request $request)
    {
    	$client = new Client;
    	$client->create($request->all());
    	return redirect('client');
    }
    //Method to update
    public function update($id, Request $request)
    {
        $client = Client::find($id);
        $client->name = $request->name;
        $client->numberDocument = $request->numberDocument;
        $client->description = $request->description;
        $client->save();
        return redirect('client');
    }
    //Method to redirect to view create
    public function create(){
    	return \View::make('clients/new');
    }
    //Method to redirect to view list
    public function index(){
        $clients = Client::all();
        return \View::make('clients/list', compact('clients'));
    }
    //Method to redirect to view update with the information of client
    public function edit($id){
        $client = Client::find($id);
        return \View::make('clients/update', compact('client'));
    }
    //Method to delete
    public function destroy($id){
        $client = Client::find($id);
        $client->delete();
        return redirect()->back();
    }
}
